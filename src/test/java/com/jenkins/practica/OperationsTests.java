package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.jenkins.utils.DogsOperations;


public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg
	 */
	@Test
	public void testRandom() {
		
		String cadena="";
		boolean bol;
		// Instantiate DogsOperations class
		DogsOperations dogso = new DogsOperations();
		cadena=dogso.getRandomDogImage();
		int num=cadena.indexOf(".jpg");
		
		if(num>=0) bol=true;
		
		assertTrue(true);
		
		if(num<0) bol=false;
		
		// Call getRandomDogImage operation and store the result on String
		
		// Assert true if the result string ends with '.jpg'
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 */
	@Test
	public void breedsList() {
		// Instantiate DogsOperations class
		
		// Call getBreedList operation and store the result on ArrayList
		
		// Assert true if the result ArrayList has size of more than 0
	}
}
